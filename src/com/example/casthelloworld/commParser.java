package com.example.casthelloworld;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

/**
 * @author Kyle Blagg <kyle.blagg @ uky.edu >
 * @version 0.9
 * @since 2014-4-19 (Since date may not be kept up to date exactly, but will be for all major iterations)
 *
 * This class handles parsing and interpreting the communications sent
 * from the Chromecast mediaRouter.  Majority of all actions take place
 * in the feedJSON function which handles the actual JSON string parsing.
 * This class also handles creating JSON string messages for replying to the
 * receiver application as well.
 */
public class commParser {
    //Action codes for the feedJSON's response
    private static final int RESETROUND = 1; //Implies ALLGOOD
    private static final int ALLGOOD = 0;
    private static final int ERROR = -1;
    private static final int DISCONNECT = 2;
    private static final int NOTJSON = 3;
    private static final int GAMESTART = 4;
    private static final int UPDATESCORE = 5;

    private String senderID; //Stores the sender apps ID incase needed.
    private JSONObject mJSONobj; //Current JSON object being worked with
    private Activity mActivity; //Used to access Activity specific resources
    private Boolean gameMode; //Flag for when the game mode has been started
    private int scoreUpdate; //Used to pass the score update
    private String winnerName; //Name of the winning player
    private int winnerScore; //Score of hte winning player
    private boolean tied; //Whether or not the winning player tied with anyone else

    /**
     * Get the winning players name
     * @return string of winning players name
     */
    public String getWinnerName() {
        return winnerName;
    }

    /**
     * Get the score of the winner
     * @return integer value of the winning score
     */
    public int getWinnerScore() {
        return winnerScore;
    }

    /**
     * Whether or not the winner tied with anyone
     * @return true means the winner tied with another player
     */
    public boolean isTied() {
        return tied;
    }

    /**
     * Set the gamemode flag for identifying whether the game has started.
     * @param gameMode
     */
    public void setGameMode(Boolean gameMode) {
        this.gameMode = gameMode;
    }

    /**
     * Returns amount of points to update the player score with.
     * @return signed integer of "points".
     */
    public int getScoreUpdate() {
        return scoreUpdate;
    }

    //Class tag for debug
    private static final String TAG = MainActivity.class.getSimpleName();

    /**
        Returns the sender app's ID
        @return sender app's ID in string form
     */
    public String getSenderID() {
        return senderID;
    }

    /**
     * Sets the sender app's ID.
     * @param senderID String form of the sender app's ID
     */
    public void setSenderID(String senderID) {
        this.senderID = senderID;
    }

    /**
     * Creates a new instance of the commParser.
     * @param inst context of the calling class.
     */
    public commParser(Activity inst){
        //Get the instance of the caller activity
        mActivity = inst;
        gameMode = false;
        scoreUpdate = 0;
        senderID = "";
    }
    /**
     * Parses JSON encoded message sent from receiver and returns action code.
     * <p>
     *     This function handles parsing the JSON encoded string sent from the
     *     receiver app, and sets the appropriate information as well as sending
     *     the caller an action code associated with the appropriate action.
     * </p>

    @param msg Contains the JSON encoded message for parsing.
    @return Action code associated with the received JSON message.
     */
    public int feedJSON(String msg){
        try{
            mJSONobj = (JSONObject) new JSONTokener(msg).nextValue(); //Convert JSON string into a JSON object
            /**
             * All JSON messages sent from the receiver application contain a "header" property,
             * which identifies what type of content is contained within the JSON encoded message.
             * Furthermore, by establishing a persistent property we remove the need to perform
             * complex analysis of the received message.
             */
            String str = mJSONobj.getString(mActivity.getString(R.string.jsonHeader));
            /*
                If the header is "start", then we are receiving the senderID / player number.
             */
            if (str.equals(mActivity.getString(R.string.header_start))){
                //Possibly add a count down timer

                setSenderID(Integer.toString(mJSONobj.getInt("id")));
                return GAMESTART;
            }
            //If header is 'question' then we are receiving a new question
            //Possible issue, we are calling the RESETROUND action and setting
            //  a new question at the same time.
            else if (str.equals(mActivity.getString(R.string.header_question))){
                if (!gameMode) {
                    ((MainActivity) mActivity).changeContentView(R.layout.activity_main);
                }
                newQuestion();
                return RESETROUND;
            }
            //If header is equal to result, then we are receiving back the correct
            //  answer for the current question.
            else if (str.equals(mActivity.getString(R.string.header_result))){
                //Parse result
                if (!gameMode){
                    ((MainActivity) mActivity).changeContentView(R.layout.activity_main);
                }
                handleResult();
                handleScore();
                return UPDATESCORE;
            }
            //If header is 'end', then the game is over and we should disconnect from
            //  the receiver and return to beginning state.
            else if (str.equals(mActivity.getString(R.string.header_end))){
                gameMode = false;
                scoreUpdate = 0;
                handleGameover();
                //Signal disconnect game is over
                return DISCONNECT;
            }
            //In the event that a header is received that wasn't expected
            //  log the header that was received and return ERROR action code.
            else{
                //This shouldn't happen...
                Log.e(TAG,"No matching cases for JSON header string");
                return ERROR;
            }
            //Error handling for JSON exceptions
        }catch(JSONException e){
            Log.d(TAG,"JSON Error: "+e.getMessage());
            return ERROR;
            //Error handling for NullPointerExceptions very common in JAVA
        }catch (NullPointerException e){
            Log.e(TAG, "Error: Null pointer\n" + e.getMessage());
            return ERROR;
            //Error handling for receiving NON-JSON encoded strings.
        }catch (ClassCastException e){
            return NOTJSON; //Special case action code. Might not be needed.
        }
    }

    /**
     * Gets the score update from the previous question.
     * <p>
     *     Reads the encoded JSON value of points to add to the
     *     users score and stores it in a temporary variable.
     * </p>
     */
    private void handleScore(){
        try {
            scoreUpdate = mJSONobj.getInt("scoreChange");
        }catch(JSONException e){
            Log.e(TAG,"JSON Error in Score handle.\nMessage: "+e.getMessage());
        }
    }

    /**
     * Set button options and question label.
     * <p>
     *     Setting the question here is poor object-oriented
     *     programming style, BUT it's faster than attempting to pass messages between
     *     java classes.
     *     JSON encoded message is decoded into multiple choice
     *     options and quiz question, which are set to the buttons and textview.
     * </p>
     */
    private void newQuestion(){
        try{
            //Get the question from the JSON object
            //  and set the question textview to the question string.
            String question = mJSONobj.getString("question");
            ((TextView)mActivity.findViewById(R.id.question_window)).setText(question);
            //Set option button's text
            setButtonText(mJSONobj.getString("option1"),R.id.option1);
            setButtonText(mJSONobj.getString("option2"),R.id.option2);
            setButtonText(mJSONobj.getString("option3"),R.id.option3);
            setButtonText(mJSONobj.getString("option4"),R.id.option4);
        //Handle JSON exception
        }catch (JSONException e){
            Log.e(TAG,"JSON Error\nMessage reads: "+e.getMessage());
        }


    }
    /**
     * Determines the correct answer to the question and updates display.
     * <p>
     *     Reads the integer value associated with the correct answer choice, and updates
     *     the display to signal which choice was correct by highlighting it in green.
     *     Furthermore, text is displayed above the option buttons informing you if you are
     *     correct or not.
     * </p>
     */
    private void handleResult(){
        try{
            //Parse the result information and determine the correct answer.
            int result = mJSONobj.getInt("answer");
            switch (result){
                case 1: //First option
                    //Set the correct answer's color to green
                    (mActivity.findViewById(R.id.option1)).setBackgroundColor(Color.GREEN);
                    //Check the selected flag on the given button
                    //If the option was the one selected by the user then set the message to correct
                    if (mActivity.findViewById(R.id.option1).isSelected()){
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.correct);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.GREEN);
                    }else{
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.wrong);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.RED);
                    }
                    break;
                case 2: //Second option
                    (mActivity.findViewById(R.id.option2)).setBackgroundColor(Color.GREEN);
                    if (mActivity.findViewById(R.id.option2).isSelected()){
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.correct);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.GREEN);
                    }else{
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.wrong);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.RED);
                    }
                    break;
                case 3: //Third option
                    (mActivity.findViewById(R.id.option3)).setBackgroundColor(Color.GREEN);
                    if (mActivity.findViewById(R.id.option3).isSelected()){
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.correct);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.GREEN);
                    }else{
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.wrong);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.RED);
                    }
                    break;
                case 4: //Fourth option
                    (mActivity.findViewById(R.id.option4)).setBackgroundColor(Color.GREEN);
                    if (mActivity.findViewById(R.id.option4).isSelected()){
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.correct);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.GREEN);
                    }else{
                        ((TextView)mActivity.findViewById(R.id.result_view)).setText(R.string.wrong);
                        ((TextView)mActivity.findViewById(R.id.result_view)).setTextColor(Color.RED);
                    }
                    break;
                default: //This shouldn't happen
                    //This would only happen if the answer option pulled from the JSON object
                    //was not encoded as an integer correctly.
                    Log.e(TAG,"Error: JSON result answer value did not match any option");
            }
            //Error handling, catch json exceptions
        }catch (JSONException e){
            Log.e(TAG,"JSON Error\nMessage reads"+e.getMessage());
        }
    }

    /**
     * Gets the game winner information
     * <p>
     *     This function retrieves the game winner
     *     information from the JSON encoded
     *     gameover message.
     *     information like player score, name,
     *     and whether or not the game was a tie
     *     are stored in the following variables
     *     for access outside the commParser class:
     *     'winnerName', 'winnerScore', and 'tied'
     * </p>
     */
    private void handleGameover(){
        try{
            winnerName = mJSONobj.getString("winner");
            winnerScore = mJSONobj.getInt("winningScore");
            tied = mJSONobj.getBoolean("tied");
        }catch(JSONException e){
            Log.e(TAG,"JSON ERROR in handleGameover()\nMessage: "+e.getMessage());
        }catch (NullPointerException e){
            Log.e(TAG,"Null Pointer Error in handleGameover()\nMessage: "+e.getMessage());
        }
    }

    /**
     * Helper function for setting the option button text.
     *
     * @param opt Contains the option string to set the button text to.
     * @param res Contains the resourceID for a specific option button.
     */
    private void setButtonText(String opt,int res){
        //Use the Activity instance to access the findViewById function.
        //Set the button text
        ((Button)mActivity.findViewById(res)).setText(opt);
    }

    /** JSON encodes a message for the receiver app
     *  <p>
            Creates a JSON encoded string message for communication
            with the receiver application.  The two string arrays passed
            into the function are expected to be parallel arrays with a one to one
            index mapping of key to value for the JSON encoded string created.
        </p>
        @param key String array containing the keys for the JSON object.
        @param val String array containing the values for the JSON object.
        @return JSON encoded message for the receiver app in string form. Null if error
     */
    public String JSONmessageBuilder(String[] key, String[] val) {
        try {
            //Insure that the String arrays are size parallel
            if (key.length != val.length) {
                return null; //Return
            } else {
                //Create JSON Object
                JSONObject jObj = new JSONObject();
                for (int i = 0; i < key.length; i++) {
                    jObj.put(key[i], val[i]);
                }
                return jObj.toString();
            }
            //Error handling for json exceptions.
        } catch (JSONException e) {
            Log.e(TAG, "Error in JSONMessageBuilder\nError Message: " + e.getMessage());
            return null;
        }
    }
}
