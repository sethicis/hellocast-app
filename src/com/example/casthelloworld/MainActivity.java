/*
 * Copyright (C) 2014 Google Inc. All Rights Reserved. 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

package com.example.casthelloworld;

import android.app.MediaRouteButton;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.MediaRouteActionProvider;
import android.support.v7.app.MediaRouteDialogFactory;
import android.support.v7.media.MediaRouteProvider;
import android.support.v7.media.MediaRouteSelector;
import android.support.v7.media.MediaRouter;
import android.support.v7.media.MediaRouter.RouteInfo;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.cast.ApplicationMetadata;
import com.google.android.gms.cast.Cast;
import com.google.android.gms.cast.Cast.ApplicationConnectionResult;
import com.google.android.gms.cast.Cast.MessageReceivedCallback;
import com.google.android.gms.cast.CastDevice;
import com.google.android.gms.cast.CastMediaControlIntent;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;

import java.io.IOException;
import java.util.ArrayList;

/**
 * @author Kyle Blagg <kyle.blagg @ uky.edu >
 * @version 0.9
 * @since 2014-4-19 (Since date may not be kept up to date exactly, but will be for all major iterations)
 *
 * This is the main activity class for the sender application for the quiz-n-cast
 * game.
 * Both the mediaRouter code for casting setup and communication are located in this file.
 * Also within this file is all the code that pertains to graphical user interface.
 * Do to a limited deadline a string encapsulation was NOT upheld.  Some graphics code can be
 * found in the commParser.java file and multiple layouts are used within a single Activity.
 */
public class MainActivity extends ActionBarActivity {

    //String for debugging messages
    private static final String TAG = MainActivity.class.getSimpleName();

    //Signal codes for the commParser returns
    private static final int RESETROUND = 1; //Implies ALLGOOD
    private static final int ALLGOOD = 0;
    private static final int ERROR = -1;
    private static final int DISCONNECT = 2; //Game over
    private static final int NOTJSON = 3;   //string passed was not in JSON form
    private static final int GAMESTART = 4; //Begin game
    private static final int UPDATESCORE = 5; //Read the commParser's updatescore value

    private static final int REQUEST_CODE = 1;
    private static final int SELECT_OPT1 = 1;
    private static final int SELECT_OPT2 = 2;
    private static final int SELECT_OPT3 = 3;
    private static final int SELECT_OPT4 = 4;

    //Majority of these date members were provided by the SDK
    private int playerScore;
    private MediaRouter mMediaRouter;
    private MediaRouteSelector mMediaRouteSelector;
    private MediaRouter.Callback mMediaRouterCallback;
    private CastDevice mSelectedDevice;
    private GoogleApiClient mApiClient;
    private Cast.Listener mCastListener;
    private ConnectionCallbacks mConnectionCallbacks;
    private ConnectionFailedListener mConnectionFailedListener;
    private commChannel mHelloWorldChannel;
    private boolean mApplicationStarted;
    private boolean mWaitingForReconnect;
    //private castHandler mCastHandler; //This data member was created for encapsulating the mediaRouter, but didn't have time to finish
    private static commParser mCommParser;     //Used to encode / decode JSON messages.
    private Menu mMenuOptions;  //Used to capture the menu instance for later use

    /**
     * Initial processes when sender app is first launched
     * <p>
         This is the onCreate function that is called immediately when the application
         starts.  Think of it like the entry point for the program. main.cpp
     * </p>
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Set the initial waiting layout until Chromecast connection is established.
        Log.d(TAG,"Calling onCreate");
        changeContentView(R.layout.waiting_for_chromecast);

        //SDK provided code
        ActionBar actionBar = getSupportActionBar();
        actionBar.setBackgroundDrawable(new ColorDrawable(
                android.R.color.transparent));

        //mCastHandler = castHandler.instance(this);

        //SDK provided code
        // Configure Cast device discovery
        mMediaRouter = MediaRouter.getInstance(getApplicationContext());
        mMediaRouteSelector = new MediaRouteSelector.Builder()
                .addControlCategory(
                        CastMediaControlIntent.categoryForCast(getResources()
                                .getString(R.string.app_id))).build();
        mMediaRouterCallback = new MyMediaRouterCallback();
        //Initialize the commParser for JSON encoding and decoding.
        mCommParser = new commParser(this);
    }

    //This function is never really used in this application right now,
    //  since we are not following good Activity segregation guide lines.
    /**
     * TODO provided by the SDK
     * Handle the voice recognition response
     * @see android.support.v4.app.FragmentActivity#onActivityResult(int, int,
     * android.content.Intent)
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            ArrayList<String> matches = data
                    .getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if (matches.size() > 0) {
                Log.d(TAG, matches.get(0));
                //sendMessage(matches.get(0));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    //TODO Provided by the SDK
    @Override
    protected void onResume() {
        super.onResume();
        // Start media router discovery
        mMediaRouter.addCallback(mMediaRouteSelector, mMediaRouterCallback,
                MediaRouter.CALLBACK_FLAG_PERFORM_ACTIVE_SCAN);
    }
    //TODO Provided by the SDK
    @Override
    protected void onPause() {
        if (isFinishing()) {
            // End media router discovery
            mMediaRouter.removeCallback(mMediaRouterCallback);
        }
        super.onPause();
    }
    //TODO Provided by the SDK
    @Override
    public void onDestroy() {
        //Commented out for debugging
        teardown();
        super.onDestroy();
    }
    //TODO Provided by the SDK
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main, menu);
        mMenuOptions = menu; //Captured for later use
        MenuItem mediaRouteMenuItem = menu.findItem(R.id.media_route_menu_item);
        MediaRouteActionProvider mediaRouteActionProvider = (MediaRouteActionProvider) MenuItemCompat
                .getActionProvider(mediaRouteMenuItem);
        // Set the MediaRouteActionProvider selector for device discovery.
        mediaRouteActionProvider.setRouteSelector(mMediaRouteSelector);

        return true;
    }

    /**
     * TODO Provided by the SDK
     * Callback for MediaRouter events
     */
    private class MyMediaRouterCallback extends MediaRouter.Callback {

        @Override
        public void onRouteSelected(MediaRouter router, RouteInfo info) {
            Log.d(TAG, "onRouteSelected");
            // Handle the user route selection.
            mSelectedDevice = CastDevice.getFromBundle(info.getExtras());
            launchReceiver();
        }
        /** This code has been edited by me, but the majority of it was written by the SDK */
        @Override
        public void onRouteUnselected(MediaRouter router, RouteInfo info) {
            Log.d(TAG, "onRouteUnselected: info=" + info);
            //ADDED: In the event that the sender app loses connection to the receiver the layout
            //  is reset back to the waiting screen until reconnected...
            //changeContentView(R.layout.waiting_for_chromecast);

            shutdown();
            teardown();
            mSelectedDevice = null;
        }
    }

    /**
     * TODO: Majority of this was provided code by the SDK, but marked parts were added
     * Start the receiver app
     */
    private void launchReceiver() {
        try {
            mCastListener = new Cast.Listener() {

                @Override
                public void onApplicationDisconnected(int errorCode) {
                    Log.d(TAG, "application has stopped");
                    //When the sender app connects to the receiver the view is set to a
                    //  waiting screen while other clients are connecting.
                    //changeContentView(R.layout.waiting_for_chromecast); //Sets layout to waiting screen
                    teardown();
                }

            };
            // Connect to Google Play services
            mConnectionCallbacks = new ConnectionCallbacks();
            mConnectionFailedListener = new ConnectionFailedListener();
            Cast.CastOptions.Builder apiOptionsBuilder = Cast.CastOptions
                    .builder(mSelectedDevice, mCastListener);
            mApiClient = new GoogleApiClient.Builder(this)
                    .addApi(Cast.API, apiOptionsBuilder.build())
                    .addConnectionCallbacks(mConnectionCallbacks)
                    .addOnConnectionFailedListener(mConnectionFailedListener)
                    .build();

            mApiClient.connect();
        } catch (Exception e) {
            Log.e(TAG, "Failed launchReceiver", e);
        }
    }

    /**
     * Sets the player's score to the new value
     */
    private void setScore(){
        TextView t = (TextView)findViewById(R.id.scoreview);
        playerScore = playerScore + mCommParser.getScoreUpdate();
        t.setText(getString(R.string.score)+" "+Integer.toString(playerScore));
    }

    /**
     *  Wrapper for the setContentView() function
     *  <p>
        This function changes the layout or "view"
        currently being used by the activity.
        Do to the risk of accessing uninitialized objects
        special test code is used to make sure that when loading a layout
        with action buttons or interactive items the proper code is loaded.
        In a way this function works as a wrapper around the built in function
        "setContentView(int layout)" which actually sets the activities layout.
        </p>
        @param layout This variable contains the resource value for the layout to load.

     */
    public void changeContentView(int layout){
        if (layout != R.layout.activity_main) {
            setContentView(layout);
        }
        else{
            mCommParser.setGameMode(true);
            setContentView(layout);
            String pNum = "Player: " + mCommParser.getSenderID();
            playerScore = 0; //Make sure the initial player score is zeroed.
            setScore(); //Set the player score
            TextView t = (TextView)findViewById(R.id.player_window);
            if (t == null) Log.e(TAG,"ERROR in layout set: Well there's your problem");
            t.setText(pNum);
            Button b = (Button) findViewById(R.id.option1);
            b.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button tmp = (Button) findViewById(R.id.option1);
                    tmp.setBackgroundColor(Color.argb(150, 0, 0, 255));
                    selectedOpt(R.id.option1);
                    sendSelection(SELECT_OPT1);
                }
            });
            b = (Button) findViewById(R.id.option2);
            b.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button tmp = (Button) findViewById(R.id.option2);
                    tmp.setBackgroundColor(Color.argb(150, 0, 0, 255));
                    selectedOpt(R.id.option2);
                    sendSelection(SELECT_OPT2);
                }
            });
            b = (Button) findViewById(R.id.option3);
            b.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button tmp = (Button) findViewById(R.id.option3);
                    tmp.setBackgroundColor(Color.argb(150, 0, 0, 255));
                    selectedOpt(R.id.option3);
                    sendSelection(SELECT_OPT3);
                }
            });
            b = (Button) findViewById(R.id.option4);
            b.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Button tmp = (Button) findViewById(R.id.option4);
                    tmp.setBackgroundColor(Color.argb(150, 0, 0, 255));
                    selectedOpt(R.id.option4);
                    sendSelection(SELECT_OPT4);
                }
            });
        }
    }
    /**
     * Handles changing the view back to initial layout
     *  <p>
        This function handles cleaning up the view before being set back to the waiting screen.
        Performing this clean up may not be necessary given the garbage collection that
        java does, but I'd rather be safe than sorry.
        The last action this function performs is calling the changeView wrapper,
        which sets the display back to the waiting screen.
        </p>
     */
    private void shutdown(){
        try {
            Log.d(TAG, "Calling shutdown...");
            Button b = (Button) findViewById(R.id.option1);
            b.setOnClickListener(null);
            b = (Button) findViewById(R.id.option2);
            b.setOnClickListener(null);
            b = (Button) findViewById(R.id.option3);
            b.setOnClickListener(null);
            b = (Button) findViewById(R.id.option4);
            b.setOnClickListener(null);
            TextView t = (TextView) findViewById(R.id.question_window);
            t.setText("Question Placeholder...");
            t = (TextView) findViewById(R.id.player_window);
            t.setText("Player Name");
            changeContentView(R.layout.waiting_for_chromecast); //Change layout back to initial
        }catch(NullPointerException e){
            Log.e(TAG,"Null Pointer Exception in shutdown()\nMessage: " + e.getMessage());
        }
    }

    /**
     * Flags the users choice for later comparison with answer
     * <p>
     *     This function is called when the user selects a possible
     *     answer from the choices provided for the given question.
     *     The users choice is set as "selected", so that it can be found later.
     *     All choice buttons are locked, so that the user can't change their mind
     *     after they make a choice.
     *     In later versions the "lock" feature may be removed, but for now it exists to
     *     avoid "race" conditions.
     * </p>
     * @param opt resource id for the selected choice.
     */
    private void selectedOpt(int opt){
        findViewById(opt).setSelected(true);
        findViewById(R.id.option1).setEnabled(false);
        findViewById(R.id.option2).setEnabled(false);
        findViewById(R.id.option3).setEnabled(false);
        findViewById(R.id.option4).setEnabled(false);
    }

    /**
     * Resets flags and visual indicators before new question
     * <p>
     *     Before a new questions can be displayed all button locks, color highlighting,
     *     and flags must be reset back to their default state.
     *     This function handles setting all the default values back for the buttons
     *     and text before displaying the next question.
     * </p>
     */
    private void resetForNextRound(){
        try {
            findViewById(R.id.option1).setBackgroundColor(getResources().getColor(R.color.common_signin_btn_light_text_default));
            findViewById(R.id.option2).setBackgroundColor(getResources().getColor(R.color.common_signin_btn_light_text_default));
            findViewById(R.id.option3).setBackgroundColor(getResources().getColor(R.color.common_signin_btn_light_text_default));
            findViewById(R.id.option4).setBackgroundColor(getResources().getColor(R.color.common_signin_btn_light_text_default));
            findViewById(R.id.option1).setEnabled(true);
            findViewById(R.id.option2).setEnabled(true);
            findViewById(R.id.option3).setEnabled(true);
            findViewById(R.id.option4).setEnabled(true);
            findViewById(R.id.option1).setSelected(false);
            findViewById(R.id.option2).setSelected(false);
            findViewById(R.id.option3).setSelected(false);
            findViewById(R.id.option4).setSelected(false);
            ((TextView) findViewById(R.id.result_view)).setText(R.string.nilstring);
        }catch(NullPointerException e){
            Log.e(TAG,"Null Pointer Error in resetForNextRound()\nMessage: " + e.getMessage());
        }
    }

    /**
     * Wrapper for the sendMessage function
     * <p>
     *     This function is a simple wrapper for the Chromecast
     *     sendMessage call, which sends a string to receiver application.
     *     Specifically this function is designed to send back the user's
     *     answer choice for the given question.
     * </p>
     * @param choice Integer value associated with the multiple choice option.
     */
    private void sendSelection(int choice){
        //Create a JSON response to send to the chromecast
        String[] key = new String[]{"header","choice"}; //JSON keys
        String[] val = new String[]{"choice",String.valueOf(choice)}; //JSON values
        //Calls the commParser function that builds the JSON message string
        String JSONResponse = mCommParser.JSONmessageBuilder(key,val);
        Log.d(TAG,"Sending Response:\n"+JSONResponse); //Log message being sent back
        sendMessage(JSONResponse); //Send message
    }

    /**
     * TODO: This code was provided by the SDK
     * Google Play services callbacks
     */
    private class ConnectionCallbacks implements
            GoogleApiClient.ConnectionCallbacks {
        @Override
        public void onConnected(Bundle connectionHint) {
            Log.d(TAG, "onConnected");

            if (mApiClient == null) {
                // We got disconnected while this runnable was pending
                // execution.
                return;
            }

            try {
                if (mWaitingForReconnect) {
                    mWaitingForReconnect = false;

                    // Check if the receiver app is still running
                    if ((connectionHint != null)
                            && connectionHint.getBoolean(Cast.EXTRA_APP_NO_LONGER_RUNNING)) {
                        Log.d(TAG, "App is no longer running");
                        teardown();
                    } else {
                        // Re-create the custom message channel
                        try {
                            Cast.CastApi.setMessageReceivedCallbacks(mApiClient,
                                    mHelloWorldChannel.getNamespace(),
                                    mHelloWorldChannel);
                        } catch (IOException e) {
                            Log.e(TAG, "Exception while creating channel", e);
                        }
                    }
                } else {
                    // Launch the receiver app
                    Cast.CastApi
                            .launchApplication(mApiClient,
                                    getString(R.string.app_id), false) //Application ID for the receiver app
                            .setResultCallback(
                                    new ResultCallback<Cast.ApplicationConnectionResult>() {
                                        @Override
                                        public void onResult(
                                                ApplicationConnectionResult result) {
                                            Status status = result.getStatus();
                                            Log.d(TAG,
                                                    "ApplicationConnectionResultCallback.onResult: statusCode"
                                                            + status.getStatusCode());
                                            if (status.isSuccess()) {
                                                //If the status was success at making a connection
                                                //   load new view.
                                                changeContentView(R.layout.wait_for_game_start); //Set view to the waiting lobby
                                                ApplicationMetadata applicationMetadata = result
                                                        .getApplicationMetadata();
                                                String sessionId = result
                                                        .getSessionId();
                                                String applicationStatus = result
                                                        .getApplicationStatus();
                                                boolean wasLaunched = result
                                                        .getWasLaunched();
                                                Log.d(TAG,
                                                        "application name: "
                                                                + applicationMetadata
                                                                        .getName()
                                                                + ", status: "
                                                                + applicationStatus
                                                                + ", sessionId: "
                                                                + sessionId
                                                                + ", wasLaunched: "
                                                                + wasLaunched);
                                                mApplicationStarted = true;

                                                // Create the custom message
                                                // channel
                                                mHelloWorldChannel = new commChannel();
                                                try {
                                                    Cast.CastApi
                                                            .setMessageReceivedCallbacks(
                                                                    mApiClient,
                                                                    mHelloWorldChannel
                                                                            .getNamespace(),
                                                                    mHelloWorldChannel);
                                                } catch (IOException e) {
                                                    Log.e(TAG,
                                                            "Exception while creating channel",
                                                            e);
                                                }

                                                // set the initial instructions
                                                //handShake(); //Send the chromecast a handshake message
                                            } else {
                                                Log.e(TAG,
                                                        "application could not launch");
                                                teardown();
                                            }
                                        }
                                    });
                }
            } catch (Exception e) {
                Log.e(TAG, "Failed to launch application", e);
            }
        }
        /**
            TODO: This code was provided by the SDK
         */
        @Override
        public void onConnectionSuspended(int cause) {
            Log.d(TAG, "onConnectionSuspended");
            mWaitingForReconnect = true;
        }
    }

    /**
     * TODO Provided by the SDK
     * Google Play services callbacks
     */
    private class ConnectionFailedListener implements
            GoogleApiClient.OnConnectionFailedListener {
        @Override
        public void onConnectionFailed(ConnectionResult result) {
            Log.e(TAG, "onConnectionFailed ");
            //Commented out for debugging
            teardown();
        }
    }

    /**
     * TODO Provided by the SDK
     * Tear down the connection to the receiver
     */
    private void teardown() {
        if (mApiClient != null) {
            if (mApplicationStarted) {
                try {
                    //Commented out because Android code may be stopping receiver application
                    //Cast.CastApi.stopApplication(mApiClient);
                    if (mHelloWorldChannel != null) {
                        Cast.CastApi.removeMessageReceivedCallbacks(mApiClient,
                                mHelloWorldChannel.getNamespace());
                        mHelloWorldChannel = null;
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Exception while removing channel", e);
                }
                mApplicationStarted = false;
            }
            if (mApiClient.isConnected()) {
                mApiClient.disconnect();
            }
            mApiClient = null;
        }
        mSelectedDevice = null;
        mWaitingForReconnect = false;
    }

    /**
     * TODO provided by SDK
     * Send a text message to the receiver
     * 
     * @param message contains the string to be sent back to the receiver app
     */
    private void sendMessage(String message) {
        if (mApiClient != null && mHelloWorldChannel != null) {
            try {
                Log.d(TAG,"Preparing message");
                Cast.CastApi.sendMessage(mApiClient,
                        mHelloWorldChannel.getNamespace(), message)
                        .setResultCallback(new ResultCallback<Status>() {
                            @Override
                            public void onResult(Status result) {
                                if (!result.isSuccess()) {
                                    Log.e(TAG, "Sending message failed");
                                }
                            }
                        });
            } catch (Exception e) {
                Log.e(TAG, "Exception while sending message", e);
            }
        } else {
            Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT)
                    .show();
        }
    }

    /**
     * TODO provided by SDK
     * Custom message channel
     */
    class commChannel implements MessageReceivedCallback {

        /**
         * TODO provided by SDK
         * @return custom namespace
         */
        public String getNamespace() {
            return getString(R.string.namespace);
        }

        /**
         * TODO Provided by SDK
         * Receive message from the receiver app
         */
        @Override
        public void onMessageReceived(CastDevice castDevice, String namespace,
                String message)
        {
            //Log the message received by from the Chromecast
            Log.d(TAG, "onMessageReceived: " + message);
            //Send the JSON string message received to the commParser for analysis.
            //Call onParseResult to determine the action to perform.
            onParseResult(mCommParser.feedJSON(message));

        }
    }

    /**
     * Handles displaying the winner
     * <p>
     *     This function handles changing the layout to the winner
     *     layout and displaying the winning score and name.
     * </p>
     */
    private void endGame(){
        changeContentView(R.layout.winner_view);
        (findViewById(R.id.done)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContentView(R.layout.waiting_for_chromecast);
                //TODO Solve the issue with mediaRouteSelector not properly updating the selected route after disconnect
                //This code attempt to update the mediaRouterButton to the current state of the code.
                try {
//
                }catch(NullPointerException e){
                    Log.e(TAG,"Null pointer error in endGame()\nError Message reads: "+e.getMessage());
                }
            }
        });
        ((TextView)findViewById(R.id.winnerscore)).setText("Score: " + mCommParser.getWinnerScore());
        if (mCommParser.isTied()){
            ((TextView)findViewById(R.id.winner_label)).setText("Winner: "+mCommParser.getWinnerName()+" Tied");
        }else{
            ((TextView)findViewById(R.id.winner_label)).setText("Winner: "+mCommParser.getWinnerName());
        }
    }

    /**
     * Determines the action to perform for the given JSON message
     * <p>
     *     commParser returns a signal code for the action indicated in
     *     the JSON message sent from the receiver app.  This function
     *     determines what the signal action was and calls the appropriate
     *     handles.
     * </p>
     * @param response Contains the signal code sent back from the commParser.
     */
    private void onParseResult(int response){
        switch (response){
            case ERROR: //Error case
                break;
            case ALLGOOD: //All good case
                break;
            case RESETROUND: //Reset buttons case
                resetForNextRound();
                break;
            case DISCONNECT: //Game over teardown
                //Commented out for testing
                teardown();
                //shutdown();
                endGame();
                //mSelectedDevice = null;
                break;
            case NOTJSON: //not JSON
                //do nothing...
                break;
            case GAMESTART: //Game start
                //Set the layout to the main game view
                //changeContentView(R.layout.activity_main);
                break;
            case UPDATESCORE: //Read the score update
                //Updates the users score
                setScore();
                break;
            default:
                //Default case
                break;
        }
    }
}
